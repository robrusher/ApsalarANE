h1. Air Native Extension for Apsalar analytics on iOS & Android

*Please note that we are no longer able to support this project and are not contributing to it.*

This is an "Adobe AIR Native Extension":http://www.adobe.com/devnet/air/native-extensions-for-air.html for "Apsalar analytics":https://apsalar.com/ on the iOS and Android platforms.

h3. Version

This is version 0.2.0 of this extension. The compiled extension contains version 6.0.8 of the Apsalar Analytics library for iOS and version 6.1.5 of the Apsalar Analytics library for Android.

h3. Binary files

The bin folder contains the compiled extension and the default swc, which can be used for local testing if required by your development environment (Flash Builder shouldn't need it, but other IDEs may).

h3. Using the extension

h4. Update your application descriptor

You'll need to include the extension in your Application Descriptor XML, and update the Android Manifest Additions with Apsalar specific data.

@<extensionID>com.sticksports.nativeExtensions.Apsalar</extensionID>@

For Android, update your Android Manifest Additions to include the following activity and receiver:

@<activity android:name="com.apsalar.sdk.Activity" android:configChanges="keyboardHidden|orientation" android:label="Apsalar"></activity>@
@<receiver android:exported="true" android:name="com.apsalar.sdk.ApsalarReceiver">@
	@<intent-filter>@
		@<action android:name="com.android.vending.INSTALL_REFERRER"></action>@
	@</intent-filter>@
@</receiver>@

h4. Start an Apsalar session

At its simplest, you initialize the extension with your api keys as follows

@Apsalar.startSession( "your api key", "your api secret" );@

You should only start the session once, after which you can log as many events as you wish.

h4. Start an Apsalar session with an opening url (iOS only)

@Apsalar.startSession( "your api key", "your api secret", "opening url" );@

Th eopening url is the url that directed iOS to open your app.

h4. Log a simple event

You may then log events with Apsalar like this

@Apsalar.logEvent( "User did something" );@

h4. Log an event with parameters

You can pass parameters through to a Apsalar event via a simple object, as follows

@Apsalar.logEvent( "Screen resolution", { width : Capabilities.screenResolutionX, height : Capabilities.screenResolutionY, dpi : Capabilities.screenDPI } );@

h4. End an Apsalar session

Ends the current session.

@Apsalar.endSession();@

h3. Developers

* Originally created by "Stick Sports":http://www.sticksports.com/
* Upgrades created by "On3 Software":http://www.on3software.com/

h2. License

This project is licensed under the "BSD license":https://github.com/StickSports/Apsalar-ANE/blob/master/LICENSE

h2. Build Instructions

h3. ANE build

From the &lt;project home&gt;/build directory, update the build.config file to match your system and certificate information. Once configured, run the Ant build.

The build contains the following targets:
all - Full build of extension
ios - Build iOS Library
android - Build android Library
ios-swc - Build iOS swc library
android-swc - Build Android swc library
default - Build default swc library
package - Create the extension package for both platforms

h3. Test build

This builds installable app packages for each platform.

From the &lt;project home&gt;/test/build directory, update the build.config file to match your system and certificate information. Once configured, run the Ant build.

The build contains the following targets:
build.debug - Build and package the app in debug mode
debug-compile - Build a debug version of the application swf
ios-package - Create the debug application .ipa package
android-package - Create the debug application .apk package